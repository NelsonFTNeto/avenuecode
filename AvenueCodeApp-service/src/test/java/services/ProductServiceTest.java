package services;

import java.io.IOException;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.openejb.jee.WebApp;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.testing.Classes;
import org.apache.openejb.testing.EnableServices;
import org.apache.openejb.testing.Module;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import entities.Product;

@EnableServices(value = "jaxrs", httpDebug = true)
@RunWith(ApplicationComposer.class)
public class ProductServiceTest {

    @Module
    @Classes(value = { ProductService.class }, cdi = true)
    public WebApp app() {
	return new WebApp().contextRoot("teste");

    }

    @Test
    public void create() throws IOException {
	Product v_Product = new Product();
	v_Product.setName("Faca");
	WebClient v_WebClient = WebClient.create(ConstantsServices.g_HttpServerUrl)
		.path("teste" + ConstantsServices.g_ProductRoot);

	v_WebClient.query("name", "Faca");
	v_WebClient.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
	Assert.assertTrue(v_WebClient.put("", Product.class).getName().equals("Faca"));

    }

}
