package app;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import services.ProductService;

public class App {

    public static void main(String[] args) throws Exception {
	// TODO Auto-generated method stub

	ServletContextHandler v_Context = new ServletContextHandler(ServletContextHandler.SESSIONS);
	v_Context.setContextPath("/");

	Server v_jettyServer = new Server(8080);
	v_jettyServer.setHandler(v_Context);

	ServletHolder v_jerseyServlet = v_Context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
	v_jerseyServlet.setInitOrder(0);

	v_jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
		ProductService.class.getCanonicalName());

	try {
	    v_jettyServer.start();
	    v_jettyServer.join();
	} finally {
	    v_jettyServer.destroy();
	}

    }

}
