package services;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import entities.ConstantsEntities;
import entities.Image;
import entities.Product;
import facades.IProductFacade;
import facades.IntImageFacade;;

@Singleton
@Lock(LockType.WRITE)
@Path("/Product")
@Produces({ MediaType.APPLICATION_JSON })
public class ProductService {

    private EntityManager m_Entity;

    private IProductFacade ProductFacade;

    private IntImageFacade ImageFacade;

    public void setEntityManager(EntityManager p_Entity) {
	m_Entity = p_Entity;
    }

    public void setProductFacade(IProductFacade p_ProductFacade) {
	this.ProductFacade = p_ProductFacade;
    }

    public void setImageFacade(IntImageFacade p_ImageFacade) {
	this.ImageFacade = p_ImageFacade;
    }

    @PostConstruct
    void init() {
	setEntityManager(
		Persistence.createEntityManagerFactory(ConstantsEntities.g_PersistenceUnitName).createEntityManager());

	setProductFacade(new facades.ProductFacade() {
	    {
		setEntityManager(m_Entity);
	    }
	});

	setImageFacade(new facades.ImageFacade() {
	    {
		setEntityManager(m_Entity);
	    }
	});

    }

    @PUT
    public Product create(@QueryParam("name") String p_Name) {

	Product v_Product = new Product();
	v_Product.setName(p_Name);

	m_Entity.getTransaction().begin();
	ProductFacade.createProduct(v_Product);
	m_Entity.getTransaction().commit();

	return ProductFacade.getByName(p_Name);

    }

    @GET
    public Product getByName(@QueryParam("name") String p_Name) {
	return ProductFacade.getByName(p_Name);
    }

    @DELETE
    public void delete(@QueryParam("id") Integer p_Id) {
	Product v_Product = ProductFacade.getById(p_Id);
	if (v_Product != null) {
	    ProductFacade.deletProduct(v_Product);
	}
    }

    @POST
    public Response update(@QueryParam("id") Integer p_Id, @QueryParam("name") String p_Name) {
	Product v_Product = ProductFacade.getById(p_Id);

	if (v_Product != null) {
	    v_Product.setName(p_Name);
	    ProductFacade.editProduct(v_Product);
	}

	return Response.ok(v_Product, MediaType.APPLICATION_XML_TYPE).build();

    }

    @POST
    @Path("/{ProductId}/Image")
    @Produces(MediaType.TEXT_PLAIN)
    public String uploadPicture(@PathParam("ProductId") Integer p_ProductId, @QueryParam("name") String p_Name,
	    @QueryParam("ext") String p_Extension, @QueryParam("path") String p_Path) {

	if (ProductFacade.getById(p_ProductId) != null) {
	    Image v_Image = new Image();
	    v_Image.setName(p_Name);

	    m_Entity.getTransaction().begin();
	    ImageFacade.uploadPicture(v_Image, new File(p_Path), p_Extension);
	    ImageFacade.saveModifications(v_Image);
	    m_Entity.getTransaction().commit();

	    return "Foto Carregada com sucesso";
	} else {
	    return "O Produto procurado nao existe";
	}

    }

    @GET
    @Path("/{ProductId}/Image")
    @Produces(MediaType.APPLICATION_XML)
    public Response getImages(@PathParam("ProductId") Integer p_ProductId) {

	Product v_Product = ProductFacade.getById(p_ProductId);

	if (v_Product != null) {
	    return Response.ok(v_Product.getImages(), MediaType.APPLICATION_XML).build();
	} else {
	    return Response.noContent().build();
	}

    }

}
