package facades;

import java.util.Collection;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import entities.Product;

@Local
public interface IProductFacade {

    public void setEntityManager(EntityManager p_EntityManager);

    public void createProduct(Product p_Product);

    public void editProduct(Product p_Product);

    public void deletProduct(Product p_Product);

    public Collection<Product> getAll();

    public Product getById(Integer p_Id);

    public Product getByName(String p_Name);

}
