package facades;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entities.Product;

@Stateless
public class ProductFacade implements IProductFacade {

    private EntityManager m_Entity;

    @Override
    public void setEntityManager(EntityManager p_Entity) {
	m_Entity = p_Entity;
    }

    @Override
    public void createProduct(Product p_Product) {
	m_Entity.persist(p_Product);
    }

    @Override
    public void editProduct(Product p_Product) {
	m_Entity.merge(p_Product);

    }

    @Override
    public void deletProduct(Product p_Product) {
	m_Entity.remove(p_Product);

    }

    @Override
    public Collection<Product> getAll() {
	TypedQuery<Product> v_query = m_Entity.createQuery("SELECT p FROM Product p", Product.class);
	return v_query.getResultList();
    }

    @Override
    public Product getById(Integer p_Id) {
	TypedQuery<Product> v_query = m_Entity.createQuery("SELECT p FROM Product p " + "WHERE p.m_Id = :p_Id",
		Product.class);
	return v_query.setParameter("p_Id", p_Id).getSingleResult();
    }

    @Override
    public Product getByName(String p_Name) {
	TypedQuery<Product> v_query = m_Entity.createQuery("SELECT p FROM Product p WHERE p.m_Name = :p_Name",
		Product.class);

	return v_query.setParameter("p_Name", p_Name).getSingleResult();

    }

}
