package facades;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entities.Image;

public class ImageFacade implements IntImageFacade {

    private EntityManager m_Entity;

    @Override
    public void uploadPicture(Image p_Image, File p_File, String p_Extension) {

	BufferedImage v_BufferImage = null;

	try {
	    v_BufferImage = ImageIO.read(p_File);
	    java.io.ByteArrayOutputStream v_Stream = new java.io.ByteArrayOutputStream();
	    ImageIO.write(v_BufferImage, p_Extension, v_Stream);
	    v_Stream.flush();
	    p_Image.setFileImage(v_Stream.toByteArray());
	    v_Stream.close();
	} catch (IOException v_Exp) {
	    v_Exp.getStackTrace();
	}

    }

    @Override
    public void saveModifications(Image p_Image) {
	TypedQuery<Image> v_Query = m_Entity.createNamedQuery("Image.FindById", Image.class);
	v_Query.setParameter("p_Id", p_Image.getId());

	if (v_Query.getSingleResult() != null) {
	    m_Entity.merge(p_Image);
	} else {
	    m_Entity.persist(p_Image);
	}

    }

    @Override
    public void setEntityManager(EntityManager p_Entity) {
	m_Entity = p_Entity;
    }

}
