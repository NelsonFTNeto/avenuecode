package facades;

import java.io.File;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import entities.Image;

@Local
public interface IntImageFacade {

    public void setEntityManager(EntityManager p_Entity);

    public void uploadPicture(Image p_Image, File p_File, String p_Extension);

    public void saveModifications(Image p_Image);

}
