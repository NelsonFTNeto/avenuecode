package facades;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLNonTransientConnectionException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.openejb.config.EjbModule;
import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.jee.StatelessBean;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.junit.Module;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entities.Image;
import entities.Product;

@RunWith(ApplicationComposer.class)
public class ProductFacadeTest {

    private static Logger m_Logger = Logger.getLogger(ProductFacadeTest.class.getName());
    private EntityManagerFactory emFactory;
    private EntityManager em;
    private Connection connection;
    private String driver = "org.apache.derby.jdbc.EmbeddedDriver";

    @Module
    public EjbModule app() throws Exception {

	final StatelessBean v_ProductFacade = new StatelessBean(ProductFacade.class);
	final StatelessBean v_ImageFacade = new StatelessBean(ImageFacade.class);
	final EjbJar v_EjbJar = new EjbJar();
	v_EjbJar.addEnterpriseBean(v_ProductFacade);
	v_EjbJar.addEnterpriseBean(v_ImageFacade);

	return new EjbModule(v_EjbJar);

    }

    @EJB
    private IProductFacade ProductFacade;

    @EJB
    private IntImageFacade ImageFacade;

    @Before
    public void setUp() throws Exception {
	try {
	    m_Logger.info("Checando base de dados");
	    Class.forName(driver).newInstance();
	    connection = DriverManager.getConnection("jdbc:derby:AvenueCodeApp;create=true", "", "");
	} catch (Exception ex) {
	    ex.getStackTrace();
	    fail("Erro de conex�o");
	}

	try {
	    m_Logger.info("Inicializando entityManager");
	    emFactory = Persistence.createEntityManagerFactory("AvenueCodeApp-model");
	    em = emFactory.createEntityManager();
	} catch (Exception ex) {
	    ex.getStackTrace();
	    fail("Erro na inicializa��o do entityManager");
	}

	ProductFacade.setEntityManager(em);
	ImageFacade.setEntityManager(em);

    }

    @After
    public void tearDown() throws Exception {
	m_Logger.info("Encerrando teste.....");

	if (em != null)
	    em.close();

	if (emFactory != null)
	    emFactory.close();

	try {
	    connection.close();
	} catch (SQLNonTransientConnectionException ex) {
	    if (ex.getErrorCode() != 4500)
		throw ex;
	}

    }

    @Test
    public void testCreateProduct() {
	Product v_Product = new Product();
	v_Product.setName("Camiseta");
	em.getTransaction().begin();
	ProductFacade.createProduct(v_Product);
	em.getTransaction().commit();

	Product v_CommitedProduct = ProductFacade.getByName("Camiseta");
	m_Logger.info(v_CommitedProduct.getId().toString() + "-" + v_CommitedProduct.getName());
	Assert.assertTrue(v_Product.getName().equals(v_CommitedProduct.getName()));
    }

    @Test
    public void testEditProduct() {

	String v_FirstName = "Bicicleta";
	String v_SecondName = "Bicicleta Infantil";
	Product v_Product1 = new Product();

	v_Product1.setName(v_FirstName);

	em.getTransaction().begin();
	ProductFacade.createProduct(v_Product1);
	Product v_DBProduct = ProductFacade.getByName(v_FirstName);
	v_DBProduct.setName(v_SecondName);
	ProductFacade.editProduct(v_DBProduct);
	em.getTransaction().commit();

	Assert.assertTrue(v_SecondName.equals(ProductFacade.getById(v_DBProduct.getId()).getName()));
    }

    @Test
    public void testDeletProduct() {
	Product v_Product = new Product();

	v_Product.setName("Bola");

	em.getTransaction().begin();
	ProductFacade.createProduct(v_Product);
	em.getTransaction().commit();

	em.getTransaction().begin();
	ProductFacade.deletProduct(v_Product);
	em.getTransaction().commit();

	Assert.assertTrue(ProductFacade.getAll().isEmpty());

    }

    @Test
    public void testGetAll() {
	m_Logger.info("teste de obter todos os dados do banco");

	Product v_Product1 = new Product();
	v_Product1.setName("Bicicleta");
	Product v_Product2 = new Product();
	v_Product2.setName("Luva");
	Product v_Product3 = new Product();
	v_Product3.setName("Torneira");

	Product[] v_Array = new Product[] { v_Product1, v_Product2, v_Product3 };

	em.getTransaction().begin();

	for (Product v_Product : v_Array) {
	    ProductFacade.createProduct(v_Product);
	}

	em.getTransaction().commit();

	List<Product> v_Products = new ArrayList<Product>(ProductFacade.getAll());
	Assert.assertTrue(v_Array.length == v_Products.size());

    }

    @Test
    public void testGetById() {
	Product v_Product = new Product();
	v_Product.setName("Raquete");

	em.getTransaction().begin();
	ProductFacade.createProduct(v_Product);
	em.getTransaction().commit();

	Product v_DBProductGetAllFirst = ProductFacade.getAll().toArray(new Product[] {})[0];
	Product v_DBProductById = ProductFacade.getById(v_DBProductGetAllFirst.getId());

	Assert.assertTrue(v_DBProductGetAllFirst.getName().equals(v_DBProductById.getName()));

    }

    @Test
    public void testInsertImageInProduct() throws IOException {

	File v_ImageFile = new File(this.getClass().getResource("/img-sample.jpg").getFile());
	Image v_ImageEntity = new Image();
	v_ImageEntity.setName("Bola");
	ImageFacade.uploadPicture(v_ImageEntity, v_ImageFile, "jpg");
	List<Image> v_Images = new ArrayList<Image>();
	v_Images.add(v_ImageEntity);

	Product v_Product = new Product();
	v_Product.setName("Bola");
	v_Product.setImages(v_Images);

	em.getTransaction().begin();
	ProductFacade.createProduct(v_Product);
	em.getTransaction().commit();

	Assert.assertArrayEquals(v_ImageEntity.getFileImage(),
		ProductFacade.getByName("Bola").getImages().get(0).getFileImage());

    }

}
