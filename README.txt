AvenueCodeApp

The project was design using maven with a parent module. Therefore, it�s possible to build all the module with the following command in the pom of the root directory

mvn clean install

To run the application and the automated tests, go the to folder AvenueCodeApp-service and run the following command

mvn jetty:run