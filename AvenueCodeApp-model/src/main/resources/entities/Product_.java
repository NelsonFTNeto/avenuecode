package entities;

import java.awt.Image;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-01-14T19:29:53.495-0200")
@StaticMetamodel(Product.class)
public class Product_ {
	public static volatile SingularAttribute<Product, Integer> m_Id;
	public static volatile SingularAttribute<Product, String> m_Name;
	public static volatile SingularAttribute<Product, Integer> m_ParentId;
	public static volatile ListAttribute<Product, Product> m_ChildProducts;
	public static volatile SingularAttribute<Product, Product> m_ParentProduct;
	public static volatile MapAttribute<Product, Integer, Image> m_Images;
}
