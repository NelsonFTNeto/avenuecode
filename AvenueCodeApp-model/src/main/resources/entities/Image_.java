package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-01-14T19:29:53.489-0200")
@StaticMetamodel(Image.class)
public class Image_ {
	public static volatile SingularAttribute<Image, Integer> m_Id;
	public static volatile SingularAttribute<Image, String> m_Name;
	public static volatile SingularAttribute<Image, byte[]> m_FileImage;
}
