package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "IMAGE")
@NamedQuery(name = "Image.FindById", query = "SELECT im FROM Image im WHERE im.m_Id = :p_Id")
@XmlRootElement(name = "image")
@XmlAccessorType(XmlAccessType.FIELD)
public class Image {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_IMAGE", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement(name = "id")
    private int m_Id;

    @Column(name = "NAME", nullable = false)
    @XmlElement(name = "name", required = true)
    private String m_Name;

    @Lob
    private byte[] m_FileImage;

    public Image() {
	super();
    }

    public int getId() {
	return m_Id;
    }

    public String getName() {
	return this.m_Name;
    }

    public void setName(String p_Name) {
	this.m_Name = p_Name;
    }

    public byte[] getFileImage() {
	return this.m_FileImage;
    }

    public void setFileImage(byte[] p_FileImage) {
	this.m_FileImage = p_FileImage;
    }

}
