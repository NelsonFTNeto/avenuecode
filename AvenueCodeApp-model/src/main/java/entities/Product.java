package entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "PRODUCT")
@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_PRODUCT", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement(name = "id")
    private Integer m_Id;

    @Column(name = "NAME", nullable = false)
    @XmlElement(name = "name", required = true)
    private String m_Name;

    @OneToMany(mappedBy = "m_ParentProduct")
    @XmlElementWrapper(name = "child-products")
    private List<Product> m_ChildProducts;

    @ManyToOne
    @JoinColumn(name = "ID_PARENT_PRODUCT", referencedColumnName = "ID_PRODUCT")
    @XmlElement(name = "parent-product")
    private Product m_ParentProduct;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true)
    @JoinColumn(name = "ID_PRODUCT")
    @XmlElementWrapper(name = "product-images")
    private List<Image> m_Images;

    public Integer getId() {
	return m_Id;
    }

    public String getName() {
	return m_Name;
    }

    public void setName(String p_Name) {
	this.m_Name = p_Name;
    }

    public List<Product> getChildProducts() {
	return m_ChildProducts;
    }

    public void setChildProducts(List<Product> p_ChildProducts) {
	this.m_ChildProducts = p_ChildProducts;
    }

    public Product getParentProduct() {
	return m_ParentProduct;
    }

    public void setParentProduct(Product p_ParentProduct) {
	this.m_ParentProduct = p_ParentProduct;
    }

    public List<Image> getImages() {
	return m_Images;
    }

    public void setImages(List<Image> p_Images) {
	this.m_Images = p_Images;
    }

    public Product() {
	super();
    }

}